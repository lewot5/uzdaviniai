package main

import (
	"flag"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

var port = ":8080"

func main() {
	r := mux.NewRouter()

	var dir string

	flag.StringVar(&dir, "dir", "./static/", "Direktorija kur yra css/html/js/uzdaviniai")
	flag.Parse()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(dir))))

	r.HandleFunc("/", Index)
	r.HandleFunc("/kategorijos/{kategorija}", Kategorijos)
	http.Handle("/", r)
	http.ListenAndServe(port, nil)
}

func Index(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./templates/index.html")
}

func Kategorijos(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	failas := fmt.Sprintf("./templates/kategorijos/%s.html", vars["kategorija"])
	http.ServeFile(w, r, failas)
}
