async function tikrintiTesta(ktg, testas) {
    let ats1 = document.getElementById(ktg+testas+'a1')
    let ats2 = document.getElementById(ktg+testas+'a2')
    let ats3 = document.getElementById(ktg+testas+'a3')
    let ats4 = document.getElementById(ktg+testas+'a4')

    let rez = document.getElementById(ktg+testas+'rez')

    let checked = -1
    if (ats1.checked) checked = 1;
    else if (ats2.checked) checked = 2;
    else if (ats3.checked) checked = 3;
    else if (ats4.checked) checked = 4;

    const resp = await fetch("/static/testai/atsakymai.json");
    const ats = await resp.json()

    if (checked == -1) {
        return;
    }

    if (ats[ktg][testas] == checked) {
        rez.className = "rez teisingai";
        rez.innerText = "Teisingai!";
    } else {
        rez.className = "rez neteisingai";
        rez.innerText = "Neteisingai!"
    }
}